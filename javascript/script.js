const navSlide = () => {

    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.navbar-links');
    const navLinks = document.querySelectorAll('.navbar-links li');


    [burger, nav].forEach(item => {
        item.addEventListener("click", () => {

            nav.classList.toggle('nav-active');

            navLinks.forEach((link, index) => {
                if (nav.contains(document.querySelector('.nav-active'))) {
                    link.style.animation = `navLinkFadeIn 0.4s ease forwards ${index / 7 + 0.7}s`;
                } else {
                    link.style.animation = `navLinkFadeOut 0.8s 1s ease forwards 1s`;
                }
            })

            burger.classList.toggle('toggle');
        })

    });

}

navSlide();